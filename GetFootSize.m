function   [Angle,L,W,Lp,Wp,pbar]=GetFootSize(footdata)
footdata=double(footdata);
[row,col]=find(footdata~=0);%找到数据中有效的点位置信息
point=[row,col];
ind=convhull(point(:,1),point(:,2));
l=length(ind);
hull=point(ind,:);          %随机点凸包
area=inf;
for i=2:l
    p1=hull(i-1,:);     %凸包上两个点
    p2=hull(i,:);   
    k1=(p1(2)-p2(2))/(p1(1)-p2(1));     %连接两点的直线，作为矩形的一条边
    b1=p1(2)-k1*p1(1);   
    d=abs(hull(:,1)*k1-hull(:,2)+b1)/sqrt(k1^2+1);  %所有凸包上的点到k1,b1直线的距离
    [h, ind]=max(d);                     %得到距离最大的点距离，即为高，同时得到该点坐标
    b2=hull(ind,2)-k1*hull(ind,1);      %相对k1,b1直线相对的另一条平行边k1,b2;
       
    k2=-1/k1;                           %以求得的直线的垂线斜率
 
    b=hull(:,2)-k2*hull(:,1);           %过凸包所有点构成的k2,b直线系
    x1=-(b1-b)/(k1-k2);                 %凸包上所有点在已求得的第一条边的投影
    y1=-(-b*k1+b1*k2)/(k1-k2);
   
    x2=-(b2-b)/(k1-k2);                 %凸包上所有点在已求得的第二条边的投影
    y2=-(-b*k1+b2*k2)/(k1-k2);
   
    [~, indmax1]=max(x1);             %投影在第一条边上x方向最大与最小值
    [~, indmin1]=min(x1);                                        
    [~, indmax2]=max(x2);             %投影在第二条边上x方向最大与最小值
    [~, indmin2]=min(x2);
   
    w=sqrt((x1(indmax1)-x1(indmin1))^2+(y1(indmax1)-y1(indmin1))^2);    %矩形的宽

    if area>=h*w                        %使面积最小
        area=h*w;
        pbar=[x1(indmax1) y1(indmax1);  %矩形四个角点
              x2(indmax2) y2(indmax2);        
              x2(indmin2) y2(indmin2);
              x1(indmin1) y1(indmin1)];            
    end
end
Lp=round(sqrt((pbar(2,1)-pbar(3,1))^2+(pbar(2,2)-pbar(3,2))^2)-0.5);%脚长（像素值）
Wp=round(sqrt((pbar(1,1)-pbar(2,1))^2+(pbar(1,2)-pbar(2,2))^2)-0.5);%脚宽（像素值）
pbar(5,:)=pbar(1,:);
%pbar=round(pbar+0.5);
%%
%将尺寸的像素值转换成实际值（cm）
L=39.0/59.0*Lp;
W=39.0/59.0*Wp;
point1=pbar(1,:);
point2=pbar(4,:);
A=point2-point1;
B=[point2(1,1)-point1(1,1),0];
Angle=acos(dot(A,B)/(norm(A)*norm(B)));%弧度
end