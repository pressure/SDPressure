%Test Update

clear;
clc;
% data1=load('1257.txt');
% rgb = label2rgb(gray2ind(uint8(data1), 255), jet(255)); 
% h=figure('Visible','off');
% imshow(rgb)
% print(h,'-djpeg','test2.jpg');

file_path='.\footdata1\';
file_path_list=dir(strcat(file_path,'*.txt'));
data=[];
file_num=length(file_path_list); 
tic;
% vidObj = VideoWriter('test3.avi');
% vidObj.FrameRate=10;
% open(vidObj);
 PressCop=[];
 GeometricCop=[];
 if file_num>0
       for  j=1:file_num
            temp=load(strcat(file_path,file_path_list(j).name));
              %将该帧数据转换为二值图像，采用形态学滤波方法,剔除孤立点
            h=im2bw(temp);
            L=bwlabeln(h,8);
            s=regionprops(L,'Area');
            bw=ismember(L,find([s.Area]>=2));
            %获取滤波后的数据
            temp=uint8(temp.*double(bw));
          %%
              %对剔除孤立点的数据进行平滑
             smoother=Filtered(temp); 
              
            %%
              
            %%
            image=imresize(uint8(smoother),5);
            [px,py]=GetPressureCop(image);
            PressCop(j,:)=[px,py];
            [gx,gy]=GetGeometricCop(image);
            GeometricCop(j,:)=[gx,gy];          
            rgb = label2rgb(gray2ind(uint8(image), 255), jet(255));
            h=figure('Visible','off');%必须要执行此步骤
            imshow(rgb);
            hold on
            plot(gy,gx,'ro');
            %2017-07-31实现GIF格式输出
            currFrame=getframe(gcf);
            nn=frame2im(currFrame);
            [nn,cm]=rgb2ind(nn,256);
            if j==1
                imwrite(nn,cm,'out.gif','gif','LoopCount',inf,'DelayTime',0.1);% 说明loopcount只是在i==1的时候才有用
            else
                imwrite(nn,cm,'out.gif','gif','WriteMode','append','DelayTime',0.1)%当i>=2的时候loopcount不起作用
            end
            %2017-07-31实现avi视频输出        
            %currFrame=getframe;
            %writeVideo(vidObj,currFrame);
           %  M(j)=getframe;
           % print(h,'-djpeg','test2.jpg');
           % data(:,:,j)=temp;
        
       end
      
 end
 % close(vidObj) ;
  toc;

