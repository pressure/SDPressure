function   S=GetEllipseArea(Pcop,pbar,C,angle)
%%通过压力中心的最小外接矩形的四个顶点，拟合出一个椭圆，即包括了所有的压力中心点
%椭圆方程ax^2 + bxy + cy^2 + dx + ey + f = 0
%输出椭圆的面积
%%
%%已知离散的点，拟合出椭圆方程，然后画出椭圆
%
XY(:,1)=pbar(:,2);
XY(:,2)=pbar(:,1);
L=sqrt((pbar(2,1)-pbar(3,1))^2+(pbar(2,2)-pbar(3,2))^2)/2.0;%长半轴（像素）
W=sqrt((pbar(1,1)-pbar(2,1))^2+(pbar(1,2)-pbar(2,2))^2)/2.0;%短半轴（像素）
S=L*W*pi/(60.0*60)*(40.0*40.0);%面积0.1658cm^2
% A = EllipseDirectFit(XY);
% %任意椭圆方程
% % A = [a b c d e f]
% %  ax^2 + bxy + cy^2 + dx + ey + f = 0
figure
plot(Pcop(:,2),Pcop(:,1),'g.')
hold on
plot(pbar(:,2),pbar(:,1),'r-');
hold on
ecc = axes2ecc(L,W);
[elat,elon] = ellipse1(C(1,2),C(1,1),[L*1.2 ecc],angle);
plot(elat,elon)
% %Convert the A to str 
% a = num2str(A(1)); 
% b = num2str(A(2)); 
% c = num2str(A(3)); 
% d = num2str(A(4)); 
% e = num2str(A(5)); 
% f = num2str(A(6));
% %Equation 
% eqt= ['(',a, ')*x^2 + (',b,')*x*y + (',c,')*y^2 + (',d,')*x+ (',e,')*y + (',f,')']; 
% xmin=0.7*min(XY(:,1)); 
% xmax=1.3*max(XY(:,2)); 
% ezplot(eqt,[xmin,xmax]);
xlim([0.8*min(XY(:,1)) 1.1*max(XY(:,1))]);
ylim([0.8*min(XY(:,2)) 1.1*max(XY(:,2))]);
xlabel('横向','FontName','LucidaSansRegular');
ylabel('纵向','FontName','LucidaSansRegular');
% title('Ellipse of Pressure Center');
title('所有压力中心点的外接椭圆','FontName','LucidaSansRegular');
% scatter(XY(:,1),XY(:,2),'r') %画出四个顶点
% hold on
% plot(Pcop(:,2),Pcop(:,1),'k-');%画出每一帧的压力中心位置
str=strcat('椭圆面积:',num2str(S),'cm^2');
gtext(str,'FontName','LucidaSansRegular');
end