clc
clear;
%给定文件所在的路径
file_path='.\sourcedata\ccjia1\';
%进行数据融合
[Pdata,FrameNums,Pcop,Gcop]=GetFusionData(file_path,false);
% figure
% rgb= label2rgb(gray2ind(uint8(Pdata), 255), jet(255)); 
% imshow(rgb);
% hold on
% plot(Pcop(:,2),Pcop(:,1),'r.');
%%
%单独将压力中心和几何中心画图展示，并计算实际的移动距离
min_x=min(Pcop(:,1));
max_x=max(Pcop(:,1));
min_y=min(Pcop(:,2));
max_y=max(Pcop(:,2));
dpx=abs(max_x-min_x);
dpy=abs(max_y-min_y);
%%波动范围
dx=dpx*40.0/60.0;%x轴移动距离0.2732cm：纵向
dy=dpy*40.0/60.0;%y轴移动距离0.7998cm:横向
%%
%压力中心移动轨迹总长度
[m1,n1]=size(Pcop);
PL=0;
dists=[];
for i=1:m1-1
    dists(i)=norm(Pcop(i+1,:)-Pcop(i,:));
    PL=PL+norm(Pcop(i+1,:)-Pcop(i,:));
end
PL=PL*40.0/60.0;%2.2418cm
figure;
t=1:1:m1;
plot(t,Pcop(t,1),'b-');%纵向
hold on
plot(t,Pcop(t,2),'r-');%横向
xlabel('t/100mm')
ylabel('Pixel value/pixel')
%gtext('\leftarrow 纵向','FontName','LucidaSansRegular');
%gtext('\leftarrow 横向','FontName','LucidaSansRegular');
set(gca, 'Fontname', 'LucidaSansRegular', 'Fontsize', 10);
legend('纵向','横向','location','best');
%title('The scope of the center of pressure oscillation');
title('足底压力中心横向和纵向的波动','FontName','LucidaSansRegular');

%%
%画出中心轨迹的椭圆，并求椭圆面积
[pbar,L,W,C,angle] =  Getminboundrect(Pcop);
%pbar=Getminboundrect(Pcop);
S=GetEllipseArea(Pcop,pbar,C,angle);
%%
%%
%对融合后的数据进行聚类分割
[Ldata,Rdata,u,re]=SplitData(Pdata);%

%%
%获得每只脚的边缘轮廓
Lboundarys=GetBoundarys(Ldata);
Rboundarys=GetBoundarys(Rdata);
%%
%分割后的数据分别保存为jpg文件到指定文件夹
rgb1 = label2rgb(gray2ind(uint8(Ldata), 255), jet(255)); 
h1=figure('Visible','off');
imshow(rgb1)
print(h1,'-djpeg','.\outdata\leftfoot.jpg');
rgb2 = label2rgb(gray2ind(uint8(Rdata), 255), jet(255)); 
h2=figure('Visible','off');
imshow(rgb2)
print(h2,'-djpeg','.\outdata\rightfoot.jpg');
%%
%分别计算左右脚的长宽
[Langle,LL,LW,LLp,LWp,Lrect]=GetFootSize(Ldata);
[Rangle,RL,RW,RLp,RWp,Rrect]=GetFootSize(Rdata);
Langle=Langle*180/pi;
Rangle=Rangle*180/pi;

%%
%对最小外接矩形进行6区域等分,获得六等份的6个分割点
[L1,L2,L3,L4,L5,L6,L7,L8]=GetSixArea(Lrect);
[R1,R2,R3,R4,R5,R6,R7,R8]=GetSixArea(Rrect);

%%
%定义六个区域多边形
[x,y]=find(Pdata~=0);
%左脚
[La1,La2,La3,La4,La5,La6]=GetAreaPolygon(Lrect,L1,L2,L3,L4,L5,L6,L7,L8);
Lin1=inpolygon(x,y,La1(:,1),La1(:,2));
Lindex1=[x(Lin1) y(Lin1)];
Lin2=inpolygon(x,y,La2(:,1),La2(:,2));
Lindex2=[x(Lin2) y(Lin2)];
Lin3=inpolygon(x,y,La3(:,1),La3(:,2));
Lindex3=[x(Lin3) y(Lin3)];
Lin4=inpolygon(x,y,La4(:,1),La4(:,2));
Lindex4=[x(Lin4) y(Lin4)];
Lin5=inpolygon(x,y,La5(:,1),La5(:,2));
Lindex5=[x(Lin5) y(Lin5)];
Lin6=inpolygon(x,y,La6(:,1),La6(:,2));
Lindex6=[x(Lin6) y(Lin6)];
%右脚
[Ra1,Ra2,Ra3,Ra4,Ra5,Ra6]=GetAreaPolygon(Rrect,R1,R2,R3,R4,R5,R6,R7,R8);
Rin1=inpolygon(x,y,Ra1(:,1),Ra1(:,2));
Rindex1=[x(Rin1) y(Rin1)];
Rin2=inpolygon(x,y,Ra2(:,1),Ra2(:,2));
Rindex2=[x(Rin2) y(Rin2)];
Rin3=inpolygon(x,y,Ra3(:,1),Ra3(:,2));
Rindex3=[x(Rin3) y(Rin3)];
Rin4=inpolygon(x,y,Ra4(:,1),Ra4(:,2));
Rindex4=[x(Rin4) y(Rin4)];
Rin5=inpolygon(x,y,Ra5(:,1),Ra5(:,2));
Rindex5=[x(Rin5) y(Rin5)];
Rin6=inpolygon(x,y,Ra6(:,1),Ra6(:,2));
Rindex6=[x(Rin6) y(Rin6)];
%%
%计算各个区域的压力总和
%left
LPs1=GetAreaSumPressures(Ldata,Lindex1);
LPs2=GetAreaSumPressures(Ldata,Lindex2);
LPs3=GetAreaSumPressures(Ldata,Lindex3);
LPs4=GetAreaSumPressures(Ldata,Lindex4);
LPs5=GetAreaSumPressures(Ldata,Lindex5);
LPs6=GetAreaSumPressures(Ldata,Lindex6);
Lsum=LPs1+LPs2+LPs3+LPs4+LPs5+LPs6;
%right
RPs1=GetAreaSumPressures(Rdata,Rindex2);
RPs2=GetAreaSumPressures(Rdata,Rindex1);
RPs3=GetAreaSumPressures(Rdata,Rindex4);
RPs4=GetAreaSumPressures(Rdata,Rindex3);
RPs5=GetAreaSumPressures(Rdata,Rindex6);
RPs6=GetAreaSumPressures(Rdata,Rindex5);
Rsum=RPs1+RPs2+RPs3+RPs4+RPs5+RPs6;
%计算各个区域压力百分比
Lpro1=sprintf('%2.2f%%',LPs1/Lsum*100);
Lpro2=sprintf('%2.2f%%',LPs2/Lsum*100);
Lpro3=sprintf('%2.2f%%',LPs3/Lsum*100);
Lpro4=sprintf('%2.2f%%',LPs4/Lsum*100);
Lpro5=sprintf('%2.2f%%',LPs5/Lsum*100);
Lpro6=sprintf('%2.2f%%',LPs6/Lsum*100);
Rpro1=sprintf('%2.2f%%',RPs1/Rsum*100);
Rpro2=sprintf('%2.2f%%',RPs2/Rsum*100);
Rpro3=sprintf('%2.2f%%',RPs3/Rsum*100);
Rpro4=sprintf('%2.2f%%',RPs4/Rsum*100);
Rpro5=sprintf('%2.2f%%',RPs5/Rsum*100);
Rpro6=sprintf('%2.2f%%',RPs6/Rsum*100);
%%
%后续对各个脚底进行分区操作
%%
data=Filtered(Pdata);
rgb=label2rgb(gray2ind(uint8(data), 255), jet(255)); 
figure('Visible','on');
imshow(rgb);
axis on;
hold on
plot(Pcop(:,2),Pcop(:,1),'r.');
hold on;
plot(Lrect(:,2),Lrect(:,1),'r-','LineWidth',1.5);
hold on
plot(Rrect(:,2),Rrect(:,1),'r-','LineWidth',1.5);
%%
%画等分线
%左脚等分线
hold on;line([L1(1,2) L3(1,2)],[L1(1,1) L3(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
hold on;line([L2(1,2) L4(1,2)],[L2(1,1) L4(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
%hold on;line([L5(1,2) L6(1,2)],[L5(1,1) L6(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
hold on;line([L7(1,2) L8(1,2)],[L7(1,1) L8(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
% gtext(Lpro1,'FontSize',10,'color','k');
% gtext(Lpro2,'FontSize',10,'color','k');
% gtext(Lpro3,'FontSize',10,'color','k');
% gtext(Lpro4,'FontSize',10,'color','k');
% gtext(Lpro5,'FontSize',10,'color','k');
% gtext(Lpro6,'FontSize',10,'color','k');
% text(15,44,Lpro1,'FontSize',10,'color','k');
% text(9,44,Lpro2,'FontSize',10,'color','k');
% text(15,32,Lpro3,'FontSize',10,'color','k');
% text(8,32,Lpro4,'FontSize',10,'color','k');
% text(15,19,Lpro5,'FontSize',10,'color','k');
% text(8,19,Lpro6,'FontSize',10,'color','k');
%右脚等分线
hold on;line([R1(1,2) R3(1,2)],[R1(1,1) R3(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
hold on;line([R2(1,2) R4(1,2)],[R2(1,1) R4(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
%hold on;line([R5(1,2) R6(1,2)],[R5(1,1) R6(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
hold on;line([R7(1,2) R8(1,2)],[R7(1,1) R8(1,1)],'Marker','none','LineStyle','-','LineWidth',1.5,'color','r');
% gtext(Rpro1,'FontSize',10,'color','k');
% gtext(Rpro2,'FontSize',10,'color','k');
% gtext(Rpro3,'FontSize',10,'color','k');
% gtext(Rpro4,'FontSize',10,'color','k');
% gtext(Rpro5,'FontSize',10,'color','k');
% gtext(Rpro6,'FontSize',10,'color','k');
% text(51,48,Rpro1,'FontSize',10,'color','k');
% text(45,48,Rpro2,'FontSize',10,'color','k');
% text(53,35,Rpro3,'FontSize',10,'color','k');
% text(47,35,Rpro4,'FontSize',10,'color','k');
% text(55,25,Rpro5,'FontSize',10,'color','k');
% text(48,25,Rpro6,'FontSize',10,'color','k');
%%
%%左脚六个区域
% hold on;
% plot(Lindex1(:,2),Lindex1(:,1),'rs')
% hold on;
% plot(Lindex2(:,2),Lindex2(:,1),'r.')
% hold on;
% plot(Lindex3(:,2),Lindex3(:,1),'k*')
% hold on;
% plot(Lindex4(:,2),Lindex4(:,1),'b+')
% hold on;
% plot(Lindex5(:,2),Lindex5(:,1),'bo')
% hold on;
% plot(Lindex6(:,2),Lindex6(:,1),'rd')
% %%
% %右脚六个区域
% hold on;
% plot(Rindex1(:,2),Rindex1(:,1),'rs')
% hold on;
% plot(Rindex2(:,2),Rindex2(:,1),'r.')
% hold on;
% plot(Rindex3(:,2),Rindex3(:,1),'k*')
% hold on;
% plot(Rindex4(:,2),Rindex4(:,1),'b+')
% hold on;
% plot(Rindex5(:,2),Rindex5(:,1),'bo')
% hold on;
% plot(Rindex6(:,2),Rindex6(:,1),'rd')
