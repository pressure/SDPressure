clear;
clc;
file_path='.\sourcedata\staticfoot\';
file_path_list=dir(strcat(file_path,'*.txt'));
file_num=length(file_path_list); 
tic;
if file_num>0
   for j=1:file_num 
            temp=load(strcat(file_path,file_path_list(j).name));%当前帧数据
            %将该帧数据转换为二值图像，采用形态学滤波方法
            h=im2bw(temp);
            L=bwlabeln(h,8);
            s=regionprops(L,'Area');
            bw=ismember(L,find([s.Area]>=2));
            %获取滤波后的数据
            temp=temp.*double(bw);
            [x,y]=meshgrid(1:60,1:60);
            z=temp;
            h=figure('Visible','off');%必须要执行此步骤
            surf(x,y,z);
            view(-150,60);
            axis off;
            colormap(prism);
            shading interp; 
            currFrame=getframe(gcf);
            nn=frame2im(currFrame);
            [nn,cm]=rgb2ind(nn,256);
            if j==1
                imwrite(nn,cm,'StaticOut3D.gif','gif','LoopCount',inf,'DelayTime',0.1);% 说明loopcount只是在i==1的时候才有用
            else
                imwrite(nn,cm,'StaticOut3D.gif','gif','WriteMode','append','DelayTime',0.1)%当i>=2的时候loopcount不起作用
            end
 end
    toc;
end