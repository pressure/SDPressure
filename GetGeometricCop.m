%2017-08-31
function [cx,cy] = GetGeometricCop( pressure )
%获得输入数据的几何中心位置坐标
%   Detailed explanation goes here
[m,n]=find(pressure~=0);
%m是对压力矩阵的行
%n是对应压力矩阵的列
m=unique(m);%去除相同的行
n=unique(n);%去除相同的列
%几何中心：
%所有有效点所在行的行标均值---几何中心的行位置
%所有有效点所在列的列表均值---几何中心的列位置
cx=sum(m)/length(m);
cy=sum(n)/length(n);
%绘制几何中心时，cy在前，cx在后: plot(cy,cx,'r*')
end

