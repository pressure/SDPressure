function [px,py,Row_sum,Col_sum,sum_p] = GetPressureCop( pressure )
%获得输入力矩阵的压力中心
%所有有效点所对应的行与有效点压力值乘积的和与所有压力值的和的比值---压力中心的行位置
%所有有效点所对应的列与有效点压力值乘积的和与所有压力值的和的比值---压力中心的列位置
[m,n]=find(pressure~=0);%找到压力矩阵中所有有效点的位置，m保存对应的行标，n保存对应的列标
%m是对压力矩阵的行
%n是对应压力矩阵的列
m=unique(m);%去除相同的行
n=unique(n);%去除相同的列
sum_p=sum(sum(pressure));%总的压力值
%求所有行
Row=[];
Row_sum=zeros(60,1);
for i=1:length(m)
    temp=pressure(m(i),:);
    Row_sum(m(i),1)=sum(temp);
    Row(i,1)=sum(temp)*m(i);    
end
sum_r=sum(Row);%有效行的所有压力和与所在行标乘积的和
Col=[];
Col_sum=zeros(1,60);
for j=1:length(n)
    temp=pressure(:,n(j));
    Col_sum(1,n(j))=sum(temp);
    Col(1,j)=sum(temp)*n(j);    
end
sum_c=sum(Col);%有效列的所有压力和与所在列标乘积的和
px=sum_r/sum_p;
py=sum_c/sum_p;
end

