function  B=Filtered(A)
p=0.25*[0.25 0.5 0.25;0.5 1 0.5;0.25 0.5 0.25];
B=filter2(p,A,'same');
end