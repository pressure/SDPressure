function   AllFrameData=ExtractAllFrameDataF3(filePath)
%RawData = load('E:\项目\压力板测试demo\处理程序\sourcedata\DynamicData\NP0001_20170807144948_Raw.txt');
RawData=load(filePath);
[m,n] = size(RawData);
for i = 1:5:n
    ii = floor(i/5) + 1;
    a(ii) = min(RawData(:,i));
end

minTime = RawData(1,1);
maxTime = RawData(m,1);
AllFrameData = zeros(60, 180, (maxTime-minTime)/10);

for ir = 1:m
    for ic = 1:5:n
        iTime     = (RawData(ir,ic) - minTime)/10 + 1;
        iNodeId   = RawData(ir,ic+1);
        iRow      = RawData(ir,ic+2) + 1;
        iCol      = RawData(ir,ic+3) + 1 + (iNodeId-1)*60;
        iPressure = RawData(ir,ic+4);
        AllFrameData(iRow,iCol,iTime) = iPressure;
    end
end
%%
%可视化
% for i=1:1:(maxTime-minTime)/10
%     pause(3);
%     rgb=label2rgb(gray2ind(uint8(AllFrameData(:,:,i)), 255), jet(255));imshow(rgb);
%    imshow(rgb);
%    hold on;
% end
end