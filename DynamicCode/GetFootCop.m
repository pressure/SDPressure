function [ax,ay,Rowsum,Colsum,sump] = GetFootCop( l,r,t,b,pressure )
%计算每一帧COP坐标以及脚印的总压力值
sump=0;
Rowsum=0;
Colsum=0;
for x=t:b
    for y=l:r
        sump=sump+pressure(x,y);
        Rowsum=Rowsum+pressure(x,y)*x;
        Colsum=Colsum+pressure(x,y)*y;
    end
end
ax=Rowsum/sump;
ay=Colsum/sump;

