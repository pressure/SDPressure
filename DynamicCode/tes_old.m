%
%%
%load data
filePath='E:\项目\压力板测试demo\处理程序\sourcedata\DynamicData\NP0001_20170807144948_Raw.txt';
 AllFrameData=ExtractAllFrameDataF3(filePath);
%%
[rowTotal,colTotal,numOfAllFrame]=size(AllFrameData);
AllFrameDilate = zeros(rowTotal, colTotal, numOfAllFrame);
AllFrameLabel  = zeros(rowTotal, colTotal, numOfAllFrame);
%每一帧不同点集与脚印集合之间的关系.
HAS_NOT_TAKEN_ANY_ACTION      = 0;
HAS_ALLOCATED_TO_EXISTED_FOOT = 1;
HAS_BEEN_MERGED_TOBE_OTHER    = 2;
HAS_NOT_CREATED_NEW_FOOT      = 3;
 %*****************************************************************
        %map操作
        %map嵌套结构体时,与想象的不一样,value不能为结构体数组.修改为结构体嵌套结构体.
        %m_mapFootDescriptor = containers.Map;
        %存放每一个脚印.
        m_mapLRFootFlag = containers.Map;
        m_mapFootDescriptor    = struct([]); %空结构体. isempty=1;
        %存放非脚印范围内点集.
        m_structFootAction     = struct;
        FootActionFrameNum     = 0;          
        rectLeftFootWholeRange.l = 0;
        rectLeftFootWholeRange.t = 0;
        rectLeftFootWholeRange.r = 0;
        rectLeftFootWholeRange.b = 0;
        
        rectRightFootWholeRange.l = 0;
        rectRightFootWholeRange.t = 0;
        rectRightFootWholeRange.r = 0;
        rectRightFootWholeRange.b = 0;
        
        rectArrayLeftFootRangeX  = zeros(1,5);
        rectArrayLeftFootRangeY  = zeros(1,5);
        rectArrayRightFootRangeX = zeros(1,5);
        rectArrayRightFootRangeY = zeros(1,5);        
        %*****************************************************************
        
        PressureCenter=zeros(numOfAllFrame,2);%存储每一帧的压力中心
        PressureColSum=zeros(numOfAllFrame,colTotal);
        PressureRowSum=zeros(rowTotal,numOfAllFrame);
        PressureSum=zeros(1,numOfAllFrame);
        %****************************************************************  
         for ii = 1:numOfAllFrame
             %%
               %计算每一帧的压力中心和每行，每列的压力值,每帧的总压力值
               [px,py,Row_sum,Col_sum,sum_p] = GetPressureCop( AllFrameData(:,:,ii) );
               PressureColSum(ii,:)=Col_sum;
               PressureRowSum(:,ii)=Row_sum;
               PressureCenter(ii,:)=[px,py];
               PressureSum(1,ii)=sum_p;
               
             %%
             
             
            %测试的时候,有几组数据,第一帧数据只有单只脚印.但是,该代码不适应这种情况.因此从3开始.
            %在编写上位机情形是,进行逻辑上的完善.--XSQ--20170213
           
            
            %标记前,膨胀下,效果应该不错.
            %膨胀做的好处:1.鞋底影响,导致断裂的情形;2.足弓不是很完整的情形.
            %左右脚距离太近,可能影响聚类分析结果.
            se1 = strel('square',3);%3*3的膨胀元素.
            AllFrameDilate(:,:,ii) = imdilate(AllFrameData(:,:,ii),se1);%膨胀
            
            %测试使用.
%             close all;
%             figure;
%             imagesc(AllFrameDenoise(:,:,ii)); %二维数组转换为彩色图像.
%             colorbar;%显示彩色分布条.颜色等级分布.
%             picFolderName  = strcat(FileDir_EachFrameLabelGeneratingPic,'\');
%             picNameForSave = strcat(num2str(ii),'.jpg');
%             saveas(gcf,[picFolderName,picNameForSave]);          
%             figure;
%             imshow(AllFrameDenoise(:,:,ii));
%             figure;
%             imshow(AllFrameDilate(:,:,ii));
%             figure;
%             imshow(AllFrameErode(:,:,ii)); 
            
            %每一帧数据,生成聚类标签.
            [AllFrameLabel(:,:,ii), ClusterNum] = bwlabel(AllFrameDilate(:,:,ii), 8); %8-邻域,连通区域标记.
            
            %%测试使用,与下面配合使用.
            close all;
            figure;
            imagesc(AllFrameLabel(:,:,ii)); %二维数组转换为彩色图像.
            %**************************************************************
            %存放每一帧点集的类别.
            m_map_cluster_attribute = struct;
            m_map=struct;%保存每一帧中每一类的数据结构体
            %**************************************************************
            %计算中心点,合并.
            for jj = 1:ClusterNum
               [row,col] = find(AllFrameLabel(:,:,ii) == jj);
               Rect.l = min(min(col));
               Rect.t = min(min(row));
               Rect.r = max(max(col));
               Rect.b = max(max(row));
               Point.x = (Rect.l + Rect.r)/2;
               Point.y = (Rect.t + Rect.b)/2;
               m_map_cluster_attribute(jj).rectWrapper   =  Rect;
               m_map_cluster_attribute(jj).cpPressure    =  Point;
               m_map_cluster_attribute(jj).clusterStatus =  HAS_NOT_TAKEN_ANY_ACTION;
               m_map_cluster_attribute(jj).index=[row,col];
               m_map_cluster_attribute(jj).map=zeros(rowTotal, colTotal);
               m_map(ii,jj).map=zeros(rowTotal, colTotal);%将第ii帧数据按照聚类进行分别存储（后期可以考虑存储index）
               for r=1:length(row)
                    m_map(ii,jj).map(row(r),col(r))=AllFrameData(row(r),col(r),ii);
               end
             %%
               %可视化测试
               points=[row,col];
               hold on
               plot(points(:,2),points(:,1),'r.')
               %%
               
               %%测试使用.从小至大扩充.rectangle('Position',[x,y,w,h])
               rectangle('position',[Rect.l, Rect.t, (Rect.r-Rect.l),(Rect.b-Rect.t)]);
               rectangle('position',[Point.x-1,Point.y-1,2,2],'curvature',[1,1],'edgecolor','r','facecolor','g');
            end
            
            %%测试使用.            
%             picFolderName  = strcat(FileDir_EachFrameLabelGeneratingPic,'\');
%             picNameForSave = strcat(num2str(ii),'.jpg');
%             saveas(gcf,[picFolderName,picNameForSave]);
            
            %生成一个完整脚印.
            %关键,先生成左右脚;确定左右脚的初始状态及范围.范围在后续使用的时候容许扩充一点.
            %1.已聚类的几个点集之间的关系.
            %2.生成脚印.
            %3.新的点集与已有脚印之间的关系.
            %1.第一层循环.已聚类点集与现有脚印之间的关系. 
            if isempty(m_mapFootDescriptor) %说明没有生成任何脚印
                            FootsIdInit     = 0;
                            clusterTypeInit = ClusterNum;
                            m_mapFootDescriptor = struct;
                            %开始未生成左脚与右脚的时候,可以这样处理.
                            %一旦生成左脚与右脚,应该直接判别.左脚与右脚各自的范围,及左右脚的总范围.不在范围内的,开始处理.
                            %如果一只脚印,是3个小类别聚合而成.这个地方存在问题？？--XSQ--20170210
                            %方法：将已有类别进行分类.如果被合并的,设置标记.
                            for kk = 1:ClusterNum-1
                                if(m_map_cluster_attribute(kk).clusterStatus == HAS_BEEN_MERGED_TOBE_OTHER)
                                    continue;
                                end

                                for ll = kk+1:ClusterNum
                                    if(m_map_cluster_attribute(ll).clusterStatus == HAS_BEEN_MERGED_TOBE_OTHER)
                                        continue;
                                    end

                                    %点集之间的距离.
                                    X_distance = abs(m_map_cluster_attribute(kk).cpPressure.x - m_map_cluster_attribute(ll).cpPressure.x);
                                    Y_distance = abs(m_map_cluster_attribute(kk).cpPressure.y - m_map_cluster_attribute(ll).cpPressure.y);
                                    Euclidean_distance = sqrt(X_distance.^2 + Y_distance.^2);

                                    %规定站立方向.Y方向聚类在一定范围的,可以合并.
                                    %开始帧,这样做肯定没有问题.但是:离开开始帧,做动作的时候,如何变化了？
                                    if(Y_distance < 10&&X_distance<30)
                                        Rect.l = min(m_map_cluster_attribute(kk).rectWrapper.l, m_map_cluster_attribute(ll).rectWrapper.l);
                                        Rect.t = min(m_map_cluster_attribute(kk).rectWrapper.t, m_map_cluster_attribute(ll).rectWrapper.t);
                                        Rect.r = max(m_map_cluster_attribute(kk).rectWrapper.r, m_map_cluster_attribute(ll).rectWrapper.r);
                                        Rect.b = max(m_map_cluster_attribute(kk).rectWrapper.b, m_map_cluster_attribute(ll).rectWrapper.b);
                                        Point.x = (Rect.l + Rect.r)/2;
                                        Point.y = (Rect.t + Rect.b)/2;

                                        %如果一只脚印,是3个小类别聚合而成.这个地方存在问题？？--XSQ--20170210
                                        %这个地方不应该产生新的类别.
                                        %clusterTypeInit = clusterTypeInit + 1;
                                        %m_map_cluster_attribute(clusterTypeInit).rectWrapper   =  Rect;
                                        %m_map_cluster_attribute(clusterTypeInit).cpPressure    =  Point;
                                        %m_map_cluster_attribute(clusterTypeInit).clusterStatus =  HAS_NOT_CREATED_NEW_FOOT;
                                        %m_map_cluster_attribute(kk).clusterStatus =  HAS_BEEN_MERGED_TOBE_OTHER;
                                        %m_map_cluster_attribute(ll).clusterStatus =  HAS_BEEN_MERGED_TOBE_OTHER;

                                        %更新第一层循环.改变第二层循环的状态.
                                        m_map_cluster_attribute(kk).rectWrapper   =  Rect;
                                        m_map_cluster_attribute(kk).cpPressure    =  Point;
                                        m_map_cluster_attribute(kk).clusterStatus =  HAS_NOT_CREATED_NEW_FOOT;

                                        m_map_cluster_attribute(ll).clusterStatus =  HAS_BEEN_MERGED_TOBE_OTHER;

                                        %从小至大扩充.rectangle('Position',[x,y,w,h])
                                       rectangle('position',[Rect.l, Rect.t, (Rect.r-Rect.l),(Rect.b-Rect.t)]);
                                       rectangle('position',[Point.x-1,Point.y-1,2,2],'curvature',[1,1],'edgecolor','r','facecolor','g');
                                    end                        
                                end
                            end

                            %第一个点集与最后一个点集类别没有与已有类别合并,更新状态.原因:都是完整脚印.%--XSQ--20160210                
                            for kk = 1:ClusterNum
                                if m_map_cluster_attribute(kk).clusterStatus ==  HAS_NOT_TAKEN_ANY_ACTION;
                                    m_map_cluster_attribute(kk).clusterStatus = HAS_NOT_CREATED_NEW_FOOT;
                                end
                            end               

                            %创建左右脚.
                            for kk = 1:ClusterNum
                                if(m_map_cluster_attribute(kk).clusterStatus ~= HAS_NOT_CREATED_NEW_FOOT)
                                  
                                    continue;
                                end                     

                                FootsIdInit = FootsIdInit + 1;
                                m_mapFootDescriptor(FootsIdInit).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                m_mapFootDescriptor(FootsIdInit).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                m_mapFootDescriptor(FootsIdInit).Untouched      = 0;
                                m_mapFootDescriptor(FootsIdInit).vtRectRange(1).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                m_mapFootDescriptor(FootsIdInit).vtRectRange(1).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                m_mapFootDescriptor(FootsIdInit).StartFrame=ii;%表示第FootsIdInit只脚印从该帧开始
                                m_mapFootDescriptor(FootsIdInit).EndFrame=0;%结束帧初始化为0
                                count(FootsIdInit)=0;
                                
                            end                
            else 
                    %有了脚印,继续扩展.取前N帧数据,作为脚印的最终范围.左脚范围,右脚范围.左右脚范围.
                     
                     FootsNum = size(m_mapFootDescriptor, 2);
                     FrameNumOfEachFoot = zeros(1,FootsNum);
                     for ff = 1:FootsNum
                            FrameNumOfEachFoot(1,ff) = size(m_mapFootDescriptor(ff).vtRectRange,2);
                     end 
                    for kk = 1:ClusterNum
                            if(m_map_cluster_attribute(kk).clusterStatus == HAS_ALLOCATED_TO_EXISTED_FOOT)
                                   continue;
                            end

                            %寻找横向距离较小的脚印.
                            FootsMergeSelected = 0;
                            X_DistanceMin      = 30;
                            Y_DistanceMin      = 10;
                            for ll = 1:FootsNum
                                Y_Distance = abs(m_mapFootDescriptor(ll).cpPressure.y - m_map_cluster_attribute(kk).cpPressure.y);
                                X_Distance = abs(m_mapFootDescriptor(ll).cpPressure.x - m_map_cluster_attribute(kk).cpPressure.x);                            
                                if Y_Distance < Y_DistanceMin && X_Distance<X_DistanceMin %条件成立 说明当前类属于现存的脚印
                                    m_map_cluster_attribute(kk).clusterStatus = HAS_ALLOCATED_TO_EXISTED_FOOT;
                                    FootsMergeSelected = ll;
                                else
                                    count(ll)=count(ll)+1;
                                end
                            end                            
                            if m_map_cluster_attribute(kk).clusterStatus == HAS_ALLOCATED_TO_EXISTED_FOOT %此类别属于其中一个被选中的脚印，即可进行合并
                                     %更新脚印范围与点集状态.
                                    %如何一个脚印分成4个类别,分别有2个类别与同一个脚印合并.脚印范围与脚印中心更新.
                                    %脚印每一帧属性更新？
                                    %脚印范围与脚印中心更新.
                                    RectFootWholeRange.l = min(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.l, m_map_cluster_attribute(kk).rectWrapper.l);
                                    RectFootWholeRange.t = min(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.t, m_map_cluster_attribute(kk).rectWrapper.t);
                                    RectFootWholeRange.r = max(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.r, m_map_cluster_attribute(kk).rectWrapper.r);
                                    RectFootWholeRange.b = max(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.b, m_map_cluster_attribute(kk).rectWrapper.b);
                                    PointFootCenter.x = (RectFootWholeRange.l + RectFootWholeRange.r)/2;
                                    PointFootCenter.y = (RectFootWholeRange.t + RectFootWholeRange.b)/2;

                                    m_mapFootDescriptor(FootsMergeSelected).rectWholeRange = RectFootWholeRange;
                                    m_mapFootDescriptor(FootsMergeSelected).cpPressure     = PointFootCenter;
                                    m_mapFootDescriptor(FootsMergeSelected).Untouched      = 0;

                                    %脚印每一帧属性更新
                                    CurFrameNum = FrameNumOfEachFoot(1,FootsMergeSelected);

                                    if size(m_mapFootDescriptor(FootsMergeSelected).vtRectRange,2) == CurFrameNum
                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                    else
                                        RectFootCurFrame.l = min(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.l, m_map_cluster_attribute(kk).rectWrapper.l);
                                        RectFootCurFrame.t = min(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.t, m_map_cluster_attribute(kk).rectWrapper.t);
                                        RectFootCurFrame.r = max(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.r, m_map_cluster_attribute(kk).rectWrapper.r);
                                        RectFootCurFrame.b = max(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.b, m_map_cluster_attribute(kk).rectWrapper.b);
                                        PointCurFrameCenter.x = (RectFootCurFrame.l + RectFootCurFrame.r)/2;
                                        PointCurFrameCenter.y = (RectFootCurFrame.t + RectFootCurFrame.b)/2;

                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange = RectFootCurFrame;
                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).cpPressure     = PointCurFrameCenter;
                                    end

                            else %当前的类别不属于任何脚印，将产生新的脚印
                                        FootsIdInit = FootsIdInit + 1;
                                        m_mapFootDescriptor(FootsIdInit).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                        m_mapFootDescriptor(FootsIdInit).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                        m_mapFootDescriptor(FootsIdInit).Untouched      = 0;
                                        m_mapFootDescriptor(FootsIdInit).vtRectRange(1).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                        m_mapFootDescriptor(FootsIdInit).vtRectRange(1).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                        FootsNum = size(m_mapFootDescriptor, 2);
                                        FrameNumOfEachFoot(1,FootsNum)=0;%开辟一个存储空间存储新的脚印所包含的帧
                                        count(FootsIdInit)=0;
                                        m_mapFootDescriptor(FootsIdInit).StartFrame=ii;%表示第FootsIdInit只脚印从该帧开始
                                        m_mapFootDescriptor(FootsIdInit).EndFrame=0;%结束帧初始化为0
                            end

                    end   
                   
                    %%判断当前帧是不是已经结束了前面的脚印
                    for t=1:FootsNum
                        if ii<numOfAllFrame
                            if count(t)==ClusterNum && m_mapFootDescriptor(t).EndFrame==0 %如果m_mapFootDescriptor(ll).EndFrame~=0，说明此脚印早已结束                           
                                m_mapFootDescriptor(t).EndFrame=ii;
                            else 
                                count(t)=0;
                            end
                        else
                            m_mapFootDescriptor(FootsNum).EndFrame=ii;
                        end
                    end 
                            
            end
         end
        
                    