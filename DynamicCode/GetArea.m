function [area] = GetArea( l,r,t,b,pressure )
%计算脚印面积，每个格子面积为4/9
sum=0;
for x=t:b
    for y=l:r
        if(pressure(x,y)>0)
            sum=sum+1;
        end
    end
end
area=sum*2/3*2/3;
