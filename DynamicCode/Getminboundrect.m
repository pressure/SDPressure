%%
%%
%计算离散点的最小外接矩形的四个顶点，并输出，矩形的长宽，和中心位置
function [pbar,L,W,C,Angle] =  Getminboundrect(data)
[r,c]=find(data~=0);
points=[r c];
ind=convhull(points(:,1),points(:,2));
l=length(ind);
hull=points(ind,:);          %随机点凸包
area=inf;
pbar=[];
for i=2:l
    p1=hull(i-1,:);     %凸包上两个点
    p2=hull(i,:);   
    k1=(p1(2)-p2(2))/(p1(1)-p2(1));     %连接两点的直线，作为矩形的一条边
    b1=p1(2)-k1*p1(1);   
    d=abs(hull(:,1)*k1-hull(:,2)+b1)/sqrt(k1^2+1);  %所有凸包上的点到k1,b1直线的距离
    [h, ind]=max(d);                     %得到距离最大的点距离，即为高，同时得到该点坐标
    b2=hull(ind,2)-k1*hull(ind,1);      %相对k1,b1直线相对的另一条平行边k1,b2;
       
    k2=-1/k1;                           %以求得的直线的垂线斜率
 
    b=hull(:,2)-k2*hull(:,1);           %过凸包所有点构成的k2,b直线系
    x1=-(b1-b)/(k1-k2);                 %凸包上所有点在已求得的第一条边的投影
    y1=-(-b*k1+b1*k2)/(k1-k2);
   
    x2=-(b2-b)/(k1-k2);                 %凸包上所有点在已求得的第二条边的投影
    y2=-(-b*k1+b2*k2)/(k1-k2);
   
    [~, indmax1]=max(x1);             %投影在第一条边上x方向最大与最小值
    [~, indmin1]=min(x1);                                        
    [~, indmax2]=max(x2);             %投影在第二条边上x方向最大与最小值
    [~, indmin2]=min(x2);
   
    w=sqrt((x1(indmax1)-x1(indmin1))^2+(y1(indmax1)-y1(indmin1))^2);    %矩形的宽

    if area>=h*w                        %使面积最小
        area=h*w;
        pbar=[x1(indmax1) y1(indmax1);  %矩形四个角点
              x2(indmax2) y2(indmax2);        
              x2(indmin2) y2(indmin2);
              x1(indmin1) y1(indmin1)];            
    end
end
[m,~]=size(pbar);
if m~=0
pbar(5,:)=pbar(1,:);
point1=pbar(1,:);
point2=pbar(4,:);
A=point2-point1;
B=[0,point2(1,2)-point1(1,2)];
Angle=acos(dot(A,B)/(norm(A)*norm(B)));%弧度
Angle=Angle*180/pi;
L=sqrt((pbar(2,1)-pbar(3,1))^2+(pbar(2,2)-pbar(3,2))^2);%脚长（像素值）
W=sqrt((pbar(1,1)-pbar(2,1))^2+(pbar(1,2)-pbar(2,2))^2);%脚宽（像素值）
[c1,~]=GetUnformPoints(pbar(1,:),pbar(3,:),2);
C=c1;
else
    pbar=0;
    L=0;
    W=0;
    Angle=0;
end

if point1(1,1)>point2(1,1)&&point1(1,2)<point2(1,2)
    Angle=Angle*(-1.0);
else if point1(1,1)>point2(1,1)&&point1(1,2)>point2(1,2)
    Angle=Angle*1.0;
    end
end