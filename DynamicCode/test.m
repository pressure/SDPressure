%
%%
function  m_map_GaitParameters=test(filePath)
%load data
%filePath='E:\项目\压力板测试demo\处理程序\sourcedata\DynamicData\NP0001_20170807144948_Raw.txt';
% filePath='F:\安大\任务\matlab压力参数\代码\pressure-SDPressure-master\SDPressure\sourcedata\DynamicData\NP0001_20170807144948_Raw.txt';
 AllFrameData=ExtractAllFrameDataF3(filePath);
%%
[rowTotal,colTotal,numOfAllFrame]=size(AllFrameData);
AllFrameDilate = zeros(rowTotal, colTotal, numOfAllFrame);
AllFrameLabel  = zeros(rowTotal, colTotal, numOfAllFrame);
%每一帧不同点集与脚印集合之间的关系.
HAS_NOT_TAKEN_ANY_ACTION      = 0;
HAS_ALLOCATED_TO_EXISTED_FOOT = 1;
HAS_BEEN_MERGED_TOBE_OTHER    = 2;
HAS_NOT_CREATED_NEW_FOOT      = 3;

%左右脚标志位
FlagRight = 1;
FlagLeft  = 2;
 %*****************************************************************
        %map操作
        %map嵌套结构体时,与想象的不一样,value不能为结构体数组.修改为结构体嵌套结构体.
        %m_mapFootDescriptor = containers.Map;
        %存放每一个脚印.
        m_mapLRFootFlag = containers.Map;
        m_mapFootDescriptor    = struct([]); %空结构体. isempty=1;
        %存放非脚印范围内点集.
        m_structFootAction     = struct;
        FootActionFrameNum     = 0;          
        rectLeftFootWholeRange.l = 0;
        rectLeftFootWholeRange.t = 0;
        rectLeftFootWholeRange.r = 0;
        rectLeftFootWholeRange.b = 0;
        
        rectRightFootWholeRange.l = 0;
        rectRightFootWholeRange.t = 0;
        rectRightFootWholeRange.r = 0;
        rectRightFootWholeRange.b = 0;
        
        rectArrayLeftFootRangeX  = zeros(1,5);
        rectArrayLeftFootRangeY  = zeros(1,5);
        rectArrayRightFootRangeX = zeros(1,5);
        rectArrayRightFootRangeY = zeros(1,5);        
        %*****************************************************************
        
        PressureCenter=zeros(numOfAllFrame,2);%存储每一帧的压力中心
        PressureColSum=zeros(numOfAllFrame,colTotal);
        PressureRowSum=zeros(rowTotal,numOfAllFrame);
        PressureSum=zeros(1,numOfAllFrame);
        %****************************************************************  
         for ii = 1:numOfAllFrame
             %%
               %计算每一帧的压力中心和每行，每列的压力值,每帧的总压力值
               [px,py,Row_sum,Col_sum,sum_p] = GetPressureCop( AllFrameData(:,:,ii) );
               PressureColSum(ii,:)=Col_sum;
               PressureRowSum(:,ii)=Row_sum;
               PressureCenter(ii,:)=[px,py];
               PressureSum(1,ii)=sum_p;
               
             %%
             
             
            %测试的时候,有几组数据,第一帧数据只有单只脚印.但是,该代码不适应这种情况.因此从3开始.
            %在编写上位机情形是,进行逻辑上的完善.--XSQ--20170213
           
            
            %标记前,膨胀下,效果应该不错.
            %膨胀做的好处:1.鞋底影响,导致断裂的情形;2.足弓不是很完整的情形.
            %左右脚距离太近,可能影响聚类分析结果.
            se1 = strel('square',3);%3*3的膨胀元素.
            AllFrameDilate(:,:,ii) = imdilate(AllFrameData(:,:,ii),se1);%膨胀
            
            %测试使用.
%             close all;
%             figure;
%             imagesc(AllFrameDenoise(:,:,ii)); %二维数组转换为彩色图像.
%             colorbar;%显示彩色分布条.颜色等级分布.
%             picFolderName  = strcat(FileDir_EachFrameLabelGeneratingPic,'\');
%             picNameForSave = strcat(num2str(ii),'.jpg');
%             saveas(gcf,[picFolderName,picNameForSave]);          
%             figure;
%             imshow(AllFrameDenoise(:,:,ii));
%             figure;
%             imshow(AllFrameDilate(:,:,ii));
%             figure;
%             imshow(AllFrameErode(:,:,ii)); 
            
            %每一帧数据,生成聚类标签.
            [AllFrameLabel(:,:,ii), ClusterNum] = bwlabel(AllFrameDilate(:,:,ii), 8); %8-邻域,连通区域标记.
            %返回一个和BW大小相同的L矩阵，包含了标记了BW中每个连通区域的类别标签，
            %这些标签的值为1、2、num（连通区域的个数）。n的值为4或8，表示是按4连通寻找区域，还是8连通寻找，默认为8。
           
            %%测试使用,与下面配合使用.
            close all;
            figure;
            imagesc(AllFrameLabel(:,:,ii)); %二维数组转换为彩色图像.
            %**************************************************************
            %存放每一帧点集的类别.
            m_map_cluster_attribute = struct;
            m_map=struct;
            %**************************************************************
            %计算中心点,合并.
            for jj = 1:ClusterNum
               [row,col] = find(AllFrameLabel(:,:,ii) == jj); %寻找标签为jj的行列值
               Rect.l = min(min(col));
               Rect.t = min(min(row));
               Rect.r = max(max(col));
               Rect.b = max(max(row));
               Point.x = (Rect.l + Rect.r)/2;
               Point.y = (Rect.t + Rect.b)/2;%求出该标签值中心点的坐标
               m_map_cluster_attribute(jj).rectWrapper   =  Rect;
               m_map_cluster_attribute(jj).cpPressure    =  Point;
               m_map_cluster_attribute(jj).clusterStatus =  HAS_NOT_TAKEN_ANY_ACTION;
               m_map_cluster_attribute(jj).index=[row,col];
               m_map_cluster_attribute(jj).map=zeros(rowTotal, colTotal);
               m_map(ii,jj).map=zeros(rowTotal, colTotal);%将第ii帧数据按照聚类进行分别存储（后期可以考虑存储index）
               for r=1:length(row)
                    m_map(ii,jj).map(row(r),col(r))=AllFrameData(row(r),col(r),ii);
               end
             %%
               %可视化测试
               %hold on 在当前图的轴（坐标系）中画了一幅图，再画另一幅图时，原来的图还在，与新图共存，都看得到
               points=[row,col];
               hold on
               plot(points(:,2),points(:,1),'r.')
               %%
               
               %%测试使用.从小至大扩充.rectangle('Position',[x,y,w,h])，绘制矩形，从x,y处绘制宽w高h矩形
               rectangle('position',[Rect.l, Rect.t, (Rect.r-Rect.l),(Rect.b-Rect.t)]);
               rectangle('position',[Point.x-1,Point.y-1,2,2],'curvature',[1,1],'edgecolor','r','facecolor','g');
            end
            
            %%测试使用.            
%             picFolderName  = strcat(FileDir_EachFrameLabelGeneratingPic,'\');
%             picNameForSave = strcat(num2str(ii),'.jpg');
%             saveas(gcf,[picFolderName,picNameForSave]);
            
            %生成一个完整脚印.
            %关键,先生成左右脚;确定左右脚的初始状态及范围.范围在后续使用的时候容许扩充一点.
            %1.已聚类的几个点集之间的关系.
            %2.生成脚印.
            %3.新的点集与已有脚印之间的关系.
            %1.第一层循环.已聚类点集与现有脚印之间的关系. 
            if isempty(m_mapFootDescriptor) %说明没有生成任何脚印
                            FootsIdInit     = 0;
                            clusterTypeInit = ClusterNum;
                            m_mapFootDescriptor = struct;
                            %开始未生成左脚与右脚的时候,可以这样处理.
                            %一旦生成左脚与右脚,应该直接判别.左脚与右脚各自的范围,及左右脚的总范围.不在范围内的,开始处理.
                            %如果一只脚印,是3个小类别聚合而成.这个地方存在问题？？--XSQ--20170210
                            %方法：将已有类别进行分类.如果被合并的,设置标记.
%%%？？？？
                            for kk = 1:ClusterNum-1
                                if(m_map_cluster_attribute(kk).clusterStatus == HAS_BEEN_MERGED_TOBE_OTHER)
                                    continue;
                                end

                                for ll = kk+1:ClusterNum
                                    if(m_map_cluster_attribute(ll).clusterStatus == HAS_BEEN_MERGED_TOBE_OTHER)
                                        continue;
                                    end

                                    %点集之间的距离.
                                    X_distance = abs(m_map_cluster_attribute(kk).cpPressure.x - m_map_cluster_attribute(ll).cpPressure.x);
                                    Y_distance = abs(m_map_cluster_attribute(kk).cpPressure.y - m_map_cluster_attribute(ll).cpPressure.y);
                                    Euclidean_distance = sqrt(X_distance.^2 + Y_distance.^2);

                                    %规定站立方向.Y方向聚类在一定范围的,可以合并.
                                    %开始帧,这样做肯定没有问题.但是:离开开始帧,做动作的时候,如何变化了？
                                    if(Y_distance < 10&&X_distance<30)
                                        Rect.l = min(m_map_cluster_attribute(kk).rectWrapper.l, m_map_cluster_attribute(ll).rectWrapper.l);
                                        Rect.t = min(m_map_cluster_attribute(kk).rectWrapper.t, m_map_cluster_attribute(ll).rectWrapper.t);
                                        Rect.r = max(m_map_cluster_attribute(kk).rectWrapper.r, m_map_cluster_attribute(ll).rectWrapper.r);
                                        Rect.b = max(m_map_cluster_attribute(kk).rectWrapper.b, m_map_cluster_attribute(ll).rectWrapper.b);
                                        Point.x = (Rect.l + Rect.r)/2;
                                        Point.y = (Rect.t + Rect.b)/2;%两个集群合并

                                        %如果一只脚印,是3个小类别聚合而成.这个地方存在问题？？--XSQ--20170210
                                        %这个地方不应该产生新的类别.
                                        %clusterTypeInit = clusterTypeInit + 1;
                                        %m_map_cluster_attribute(clusterTypeInit).rectWrapper   =  Rect;
                                        %m_map_cluster_attribute(clusterTypeInit).cpPressure    =  Point;
                                        %m_map_cluster_attribute(clusterTypeInit).clusterStatus =  HAS_NOT_CREATED_NEW_FOOT;
                                        %m_map_cluster_attribute(kk).clusterStatus =  HAS_BEEN_MERGED_TOBE_OTHER;
                                        %m_map_cluster_attribute(ll).clusterStatus =  HAS_BEEN_MERGED_TOBE_OTHER;

                                        %更新第一层循环.改变第二层循环的状态.
                                        m_map_cluster_attribute(kk).rectWrapper   =  Rect;
                                        m_map_cluster_attribute(kk).cpPressure    =  Point;
                                        m_map_cluster_attribute(kk).clusterStatus =  HAS_NOT_CREATED_NEW_FOOT;

                                        m_map_cluster_attribute(ll).clusterStatus =  HAS_BEEN_MERGED_TOBE_OTHER;

                                        %从小至大扩充.rectangle('Position',[x,y,w,h])
                                       rectangle('position',[Rect.l, Rect.t, (Rect.r-Rect.l),(Rect.b-Rect.t)]);
                                       rectangle('position',[Point.x-1,Point.y-1,2,2],'curvature',[1,1],'edgecolor','r','facecolor','g');
                                    end                        
                                end
                            end
%？？？
                            %第一个点集与最后一个点集类别没有与已有类别合并,更新状态.原因:都是完整脚印.%--XSQ--20160210                
                            for kk = 1:ClusterNum
                                if m_map_cluster_attribute(kk).clusterStatus ==  HAS_NOT_TAKEN_ANY_ACTION;
                                    m_map_cluster_attribute(kk).clusterStatus = HAS_NOT_CREATED_NEW_FOOT;
                                end
                            end               

                            %创建左右脚.
                            for kk = 1:ClusterNum
                                if(m_map_cluster_attribute(kk).clusterStatus ~= HAS_NOT_CREATED_NEW_FOOT)
                                  
                                    continue;
                                end                     
%？？？
                                FootsIdInit = FootsIdInit + 1;
                                m_mapFootDescriptor(FootsIdInit).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                m_mapFootDescriptor(FootsIdInit).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                m_mapFootDescriptor(FootsIdInit).Untouched      = 0;
                                m_mapFootDescriptor(FootsIdInit).vtRectRange(1).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                m_mapFootDescriptor(FootsIdInit).vtRectRange(1).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                               % m_mapFootDescriptor(FootsIdInit).containers(ii,kk)=1;
                                m_mapFootDescriptor(FootsIdInit).StartFrame=ii;%表示第FootsIdInit只脚印从该帧开始
                                m_mapFootDescriptor(FootsIdInit).EndFrame=0;%结束帧初始化为0
                                count(FootsIdInit)=0;
                                
                            end                
            else 
                    %有了脚印,继续扩展.取前N帧数据,作为脚印的最终范围.左脚范围,右脚范围.左右脚范围.
                     
                     FootsNum = size(m_mapFootDescriptor, 2);
                     FrameNumOfEachFoot = zeros(1,FootsNum);
                     for ff = 1:FootsNum
                            FrameNumOfEachFoot(1,ff) = size(m_mapFootDescriptor(ff).vtRectRange,2);
                     end 
                    for kk = 1:ClusterNum
                            if(m_map_cluster_attribute(kk).clusterStatus == HAS_ALLOCATED_TO_EXISTED_FOOT)
                                   continue;
                            end

                            %寻找横向距离较小的脚印.
                            FootsMergeSelected = 0;
                            X_DistanceMin      = 30;
                            Y_DistanceMin      = 10;
                            for ll = 1:FootsNum
                                Y_Distance = abs(m_mapFootDescriptor(ll).cpPressure.y - m_map_cluster_attribute(kk).cpPressure.y);
                                X_Distance = abs(m_mapFootDescriptor(ll).cpPressure.x - m_map_cluster_attribute(kk).cpPressure.x);                            
                                if Y_Distance < Y_DistanceMin && X_Distance<X_DistanceMin %条件成立 说明当前类属于现存的脚印
                                    m_map_cluster_attribute(kk).clusterStatus = HAS_ALLOCATED_TO_EXISTED_FOOT;
                                    FootsMergeSelected = ll;
                                else
                                    count(ll)=count(ll)+1;
                                end
                            end                            
                            if m_map_cluster_attribute(kk).clusterStatus == HAS_ALLOCATED_TO_EXISTED_FOOT %此类别属于其中一个被选中的脚印，即可进行合并
                                     %更新脚印范围与点集状态.
                                    %如何一个脚印分成4个类别,分别有2个类别与同一个脚印合并.脚印范围与脚印中心更新.
                                    %脚印每一帧属性更新？
                                    %脚印范围与脚印中心更新.
                                    RectFootWholeRange.l = min(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.l, m_map_cluster_attribute(kk).rectWrapper.l);
                                    RectFootWholeRange.t = min(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.t, m_map_cluster_attribute(kk).rectWrapper.t);
                                    RectFootWholeRange.r = max(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.r, m_map_cluster_attribute(kk).rectWrapper.r);
                                    RectFootWholeRange.b = max(m_mapFootDescriptor(FootsMergeSelected).rectWholeRange.b, m_map_cluster_attribute(kk).rectWrapper.b);
                                    PointFootCenter.x = (RectFootWholeRange.l + RectFootWholeRange.r)/2;
                                    PointFootCenter.y = (RectFootWholeRange.t + RectFootWholeRange.b)/2;

                                    m_mapFootDescriptor(FootsMergeSelected).rectWholeRange = RectFootWholeRange;
                                    m_mapFootDescriptor(FootsMergeSelected).cpPressure     = PointFootCenter;
                                    m_mapFootDescriptor(FootsMergeSelected).Untouched      = 0;

                                    %脚印每一帧属性更新
                                    CurFrameNum = FrameNumOfEachFoot(1,FootsMergeSelected);

                                    if size(m_mapFootDescriptor(FootsMergeSelected).vtRectRange,2) == CurFrameNum
                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                    else
                                        RectFootCurFrame.l = min(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.l, m_map_cluster_attribute(kk).rectWrapper.l);
                                        RectFootCurFrame.t = min(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.t, m_map_cluster_attribute(kk).rectWrapper.t);
                                        RectFootCurFrame.r = max(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.r, m_map_cluster_attribute(kk).rectWrapper.r);
                                        RectFootCurFrame.b = max(m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange.b, m_map_cluster_attribute(kk).rectWrapper.b);
                                        PointCurFrameCenter.x = (RectFootCurFrame.l + RectFootCurFrame.r)/2;
                                        PointCurFrameCenter.y = (RectFootCurFrame.t + RectFootCurFrame.b)/2;

                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).rectWholeRange = RectFootCurFrame;
                                        m_mapFootDescriptor(FootsMergeSelected).vtRectRange(CurFrameNum+1).cpPressure     = PointCurFrameCenter;
                                    end

                            else %当前的类别不属于任何脚印，将产生新的脚印
                                        FootsIdInit = FootsIdInit + 1;
                                        m_mapFootDescriptor(FootsIdInit).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                        m_mapFootDescriptor(FootsIdInit).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                        m_mapFootDescriptor(FootsIdInit).Untouched      = 0;
                                        m_mapFootDescriptor(FootsIdInit).vtRectRange(1).rectWholeRange = m_map_cluster_attribute(kk).rectWrapper;
                                        m_mapFootDescriptor(FootsIdInit).vtRectRange(1).cpPressure     = m_map_cluster_attribute(kk).cpPressure;
                                        FootsNum = size(m_mapFootDescriptor, 2);
                                        FrameNumOfEachFoot(1,FootsNum)=0;%开辟一个存储空间存储新的脚印所包含的帧
                                        count(FootsIdInit)=0;
                                        m_mapFootDescriptor(FootsIdInit).StartFrame=ii;%表示第FootsIdInit只脚印从该帧开始
                                        m_mapFootDescriptor(FootsIdInit).EndFrame=0;%结束帧初始化为0
                            end

                    end   
                   
                    %%判断当前帧是不是已经结束了前面的脚印
                    for t=1:FootsNum
                        if ii<numOfAllFrame
                            if count(t)==ClusterNum && m_mapFootDescriptor(t).EndFrame==0 %如果m_mapFootDescriptor(ll).EndFrame~=0，说明此脚印早已结束                           
                                m_mapFootDescriptor(t).EndFrame=ii;
                            else 
                                count(t)=0;
                            end
                        else
                            m_mapFootDescriptor(FootsNum).EndFrame=ii;
                        end
                    end 
                            
            end
         end
       %%判别左右脚印,根据站立方向，如果中心点的y坐标前一个大于后一个则是右脚的脚印，1代表右脚，2代表左脚；
       % m_mapRightFootDescriptor = struct{[]};
       %  m_mapLefttFootDescriptor = struct{[]}; 
         
         for aa=1:FootsNum-1
             if(m_mapFootDescriptor(aa).cpPressure.y > m_mapFootDescriptor(aa +1 ).cpPressure.y)
                 m_mapFootDescriptor(aa).footflag = FlagRight;
                 m_mapFootDescriptor(aa+1).footflag = FlagLeft; 
                
             else
                  m_mapFootDescriptor(aa).footflag = FlagLeft;
                  m_mapFootDescriptor(aa+1).footflag = FlagRight; 
                     
             end
         end
         
         %计算步态参数
         m_map_GaitParameters = struct;
         m_map_GaitParameters.stride=0;
         SumOfstepwidth=0;
         %判断是否大于三个脚印
         %计算步长步幅。
         if(FootsNum>3)
             for aa = 1:2
               m_map_GaitParameters.stride=(m_map_GaitParameters.stride+(m_mapFootDescriptor(aa+1).cpPressure.x-m_mapFootDescriptor(aa).cpPressure.x))*120/180;
             end
             m_map_GaitParameters.steplength=m_map_GaitParameters.stride/(FootsNum-2);
         else
             m_map_GaitParameters.steplength=(m_mapFootDescriptor(2).cpPressure.x-m_mapFootDescriptor(1).cpPressure.x)*120/180;
             m_map_GaitParameters.stride=m_map_GaitParameters.steplength*2;%如果小于三个脚印，则步幅就按步长的两倍计算
         end
         %计算步宽和速度
         for bb=1:FootsNum-1
             SumOfstepwidth=(SumOfstepwidth+abs(m_mapFootDescriptor(aa+1).cpPressure.y-m_mapFootDescriptor(aa).cpPressure.y))*40/60;
         end
         m_map_GaitParameters.stepwidth = SumOfstepwidth /(FootsNum-1);%计算平均步宽，
         m_map_GaitParameters.speed=(((m_mapFootDescriptor(FootsNum-1).cpPressure.x-m_mapFootDescriptor(1).cpPressure.x)*120/180)/100)/((m_mapFootDescriptor(FootsNum-1).EndFrame-m_mapFootDescriptor(1).StartFrame)*0.01);
         %计算步态周期，若脚印小于三个，则计算不出，将周期社为0
         %计算支撑相和摆动相
         if(FootsNum>3)
             m_map_GaitParameters.cycle=(m_mapFootDescriptor(3).StartFrame-m_mapFootDescriptor(1).StartFrame)*10;
             m_map_GaitParameters.StancePhase=(m_mapFootDescriptor(1).EndFrame-m_mapFootDescriptor(1).StartFrame)*10;
             m_map_GaitParameters.SwingPhase=(m_mapFootDescriptor(3).StartFrame-m_mapFootDescriptor(1).EndFrame)*10;
         else
             m_map_GaitParameters.cycle=0;
         end
         SumTimeOfRight=0;
         SumTimeOfLeft=0;
         SumLengthOfRight=0;
         SumLengthOfLeft=0;
         RightNum=0;
         LeftNum=0;
         %计算左右脚着地时间以及左右脚脚长
         %由于最后一个脚印可能为不完整脚印，所以最后一个脚印不计算，取其他脚印平均值
         for cc=1:FootsNum-1
             if(m_mapFootDescriptor(cc).footflag==FlagRight)
                 SumTimeOfRight=SumTimeOfRight+(m_mapFootDescriptor(cc).EndFrame-m_mapFootDescriptor(cc).StartFrame)*10;
                 RightNum=RightNum+1;
                 SumLengthOfRight=SumLengthOfRight+(m_mapFootDescriptor(cc).rectWholeRange.r-m_mapFootDescriptor(cc).rectWholeRange.l)*120/180;
             else
                 SumTimeOfLeft=SumTimeOfLeft+(m_mapFootDescriptor(cc).EndFrame-m_mapFootDescriptor(cc).StartFrame)*10;
                 LeftNum=LeftNum+1;
                 SumLengthOfLeft=SumLengthOfLeft+(m_mapFootDescriptor(cc).rectWholeRange.r-m_mapFootDescriptor(cc).rectWholeRange.l)*120/180;
             end
         end
         m_map_GaitParameters.RightTime=SumTimeOfRight/RightNum;
         m_map_GaitParameters.LeftTime=SumTimeOfLeft/LeftNum;
         m_map_GaitParameters.FootLengtOfRight=SumLengthOfRight/RightNum;
         m_map_GaitParameters.FootLengtOfLeft=SumLengthOfLeft/LeftNum;
         %计算双支撑相时间
         if(m_mapFootDescriptor(1).EndFrame>m_mapFootDescriptor(2).StartFrame)
             m_map_GaitParameters.DoubleSupportPhase=(m_mapFootDescriptor(1).EndFrame-m_mapFootDescriptor(2).StartFrame)*10;
         else
             m_map_GaitParameters.DoubleSupportPhase=0;
         end
         %计算左右脚的COP总长
         numOfFrame=m_mapFootDescriptor(1).EndFrame-m_mapFootDescriptor(1).StartFrame;
         FirstFootCOP=zeros(numOfFrame,2);%存储脚1每一帧的压力中心
         FirstFootSumPressure=zeros(1,numOfFrame);
         %Farea=0;
         for dd = m_mapFootDescriptor(1).StartFrame:m_mapFootDescriptor(1).EndFrame-1
             %%
               %计算每一帧的压力中心和每行，每列的压力值,每帧的总压力值
               l1=m_mapFootDescriptor(1).rectWholeRange.l;
               r1=m_mapFootDescriptor(1).rectWholeRange.r;
               t1=m_mapFootDescriptor(1).rectWholeRange.t;
               
               b1=m_mapFootDescriptor(1).rectWholeRange.b;
               
               [ax,ay,Rowsum,Colsum,sump] = GetFootCop( l1,r1,t1,b1,AllFrameData(:,:,dd) );
               %[Farea]=GetArea(l1,r1,t1,b1,AllFrameData(:,:,dd));
               %Firstarea=Farea;
               FirstFootCOP(dd,:)=[ax,ay];
               
               FirstFootSumPressure(1,dd)=sump;       
%                PressureColSum(ii,:)=Colsum;
%                PressureRowSum(:,ii)=Rowsum;
%                PressureCenter(ii,:)=[ax,ay];
%                PressureSum(1,ii)=sum_p;
         end
         FirstSumCop=0;
         for ff=1:numOfFrame-1
             FirstSumCop=FirstSumCop+(sqrt((FirstFootCOP(ff+1,1)-FirstFootCOP(ff,1))^2+(FirstFootCOP(ff+1,2)-FirstFootCOP(ff,2))^2))*2/3;
         end
         SecondnumOfFrame=m_mapFootDescriptor(2).EndFrame-m_mapFootDescriptor(2).StartFrame;
         %计算脚2的COP总长
         SecondFootCOP=zeros(SecondnumOfFrame,2);%存储脚2每一帧的压力中心
         SecondFootSumPressure=zeros(1,SecondnumOfFrame);
         xx=0;
         for tt = m_mapFootDescriptor(2).StartFrame:m_mapFootDescriptor(2).EndFrame-1
             %%
               %计算每一帧的压力中心和每行，每列的压力值,每帧的总压力值
               xx=xx+1;
               l2=m_mapFootDescriptor(2).rectWholeRange.l;
               r2=m_mapFootDescriptor(2).rectWholeRange.r;
               t2=m_mapFootDescriptor(2).rectWholeRange.t;
               
               b2=m_mapFootDescriptor(2).rectWholeRange.b;
               
               [ax2,ay2,Rowsum2,Colsum2,sump2] = GetFootCop( l2,r2,t2,b2,AllFrameData(:,:,tt) );
               SecondFootCOP(xx,:)=[ax2,ay2];
               
               SecondFootSumPressure(1,xx)=sump2;       
%                PressureColSum(ii,:)=Colsum;
%                PressureRowSum(:,ii)=Rowsum;
%                PressureCenter(ii,:)=[ax,ay];
%                PressureSum(1,ii)=sum_p;
         end
         SecondSumCop=0;
         for ee=1:SecondnumOfFrame-1
             SecondSumCop=SecondSumCop+(sqrt((SecondFootCOP(ee+1,1)-SecondFootCOP(ee,1))^2+(SecondFootCOP(ee+1,2)-SecondFootCOP(ee,2))^2))*2/3;
         end
         %将第二个脚印结束帧之前的所有帧累加在一起
         %用于计算脚印面积
         SumPressure=zeros(rowTotal, colTotal);
         for ii=1:m_mapFootDescriptor(2).EndFrame-1
             SumPressure=SumPressure+AllFrameData(:,:,ii);
         end
         [Farea]=GetArea(l1,r1,t1,b1,SumPressure);
         Firstarea=Farea;
         [Sarea]=GetArea(l2,r2,t2,b2,SumPressure);
         Secondarea=Sarea;
         %根据左右脚标志变量，来判断以及确定左右脚COP总长
         %判断左右脚脚印面积
         if(m_mapFootDescriptor(1).footflag==FlagRight)
             m_map_GaitParameters.RightCOP=FirstSumCop;
             m_map_GaitParameters.LeftCOP=SecondSumCop;
             m_map_GaitParameters.RightArea=Firstarea;
             m_map_GaitParameters.LeftArea=Secondarea;
         else
             m_map_GaitParameters.RightCOP=SecondSumCop;
             m_map_GaitParameters.LeftCOP=FirstSumCop;
             m_map_GaitParameters.RightArea=Secondarea;
             m_map_GaitParameters.LeftArea=Firstarea;
             
         end

 
         
end   
         
         
         
         
         
         
        
                    