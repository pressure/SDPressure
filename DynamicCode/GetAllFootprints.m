function  image=GetAllFootprints(AllFrameData)
% filePath='E:\项目\压力板测试demo\处理程序\sourcedata\DynamicData\NP0001_20170807144948_Raw.txt';
% AllFrameData=ExtractAllFrameDataF3(filePath);
[row,col,frame]=size(AllFrameData);
Data=zeros(row,col);%保存结果
for f=1:1:frame
   [r,c]=find(AllFrameData(:,:,f)~=0); 
   for j=1:length(r)
       Data(r(j),c(j))=AllFrameData(r(j),c(j),f);
   end    
end

%%
image=imresize(Data,2);
image=label2rgb(gray2ind(uint8(image), 255), jet(255));
%可视化
% figure;
% imshow(image);


end