function   BwFrames=GetBwAllFrames(AllFrameData)
[m,n,f]=size(AllFrameData);
% BwFrames=zeros(m,n,f);
for i=1:1:f
   image=AllFrameData(:,:,i);
   BwFrames(:,:,i)=im2bw(image);
end


end