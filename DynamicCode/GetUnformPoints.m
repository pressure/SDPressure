function  [p1,p2]=GetUnformPoints(point1,point2,num)
%%求给定两个点的中点和三等分的两个点
%%
%三等分
if num==3
if point1(1,1)>point2(1,1)
x1=point1(1,1)-abs(point2(1,1)-point1(1,1))/3.0;
x2=point1(1,1)-2*abs(point2(1,1)-point1(1,1))/3.0;
else
x1=point1(1,1)+abs(point2(1,1)-point1(1,1))/3.0;
x2=point1(1,1)+2*abs(point2(1,1)-point1(1,1))/3.0;
end
if point1(1,2)<point2(1,2)
y1=point1(1,2)+abs(point2(1,2)-point1(1,2))/3.0;
y2=point1(1,2)+2*abs(point2(1,2)-point1(1,2))/3.0;
else
y1=point1(1,2)-abs(point2(1,2)-point1(1,2))/3.0;
y2=point1(1,2)-2*abs(point2(1,2)-point1(1,2))/3.0;
end
p1=[x1 y1];
p2=[x2 y2];
end
%%
%二等分
if num==2
  if point1(1,1)>point2(1,1)
      x=point1(1,1)-abs(point2(1,1)-point1(1,1))/2.0;
  else
      x=point1(1,1)+abs(point2(1,1)-point1(1,1))/2.0;
  end
  if point1(1,2)<point2(1,2)
      y=point1(1,2)+abs(point2(1,2)-point1(1,2))/2.0;
  else
      y=point1(1,2)-abs(point2(1,2)-point1(1,2))/2.0;
  end
  p1=[x y];
  p2=p1;
end
end