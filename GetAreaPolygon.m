function [rect1,rect2,rect3,rect4,rect5,rect6]=GetAreaPolygon(MinRect,f1,f2,f3,f4,f5,f6,f7,f8)

rect1=[MinRect(1,:);f7;f5;f1;MinRect(1,:)];
rect2=[f7;MinRect(2,:);f3;f5;f7];
rect3=[f1;f5;f6;f2;f1];
rect4=[f5;f3;f4;f6;f5];
rect5=[f2;f6;f8;MinRect(4,:);f2];
rect6=[f6;f4;MinRect(3,:);f8;f6];
end