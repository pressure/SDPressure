function  [image,footdata,PressCop,GeometricCop]=GetTotalPressureImage(file_path)
%file_path='.\footdata1\';
file_path_list=dir(strcat(file_path,'*.txt'));
file_num=length(file_path_list); 
 PressCop=[];
 GeometricCop=[];
 number=uint8(zeros(60,60));%统计每个压力点在采样时间内含有多少个数据值
 p_value=uint8(zeros(60,60));
for  j=100:file_num-130
            temp=load(strcat(file_path,file_path_list(j).name));
              %将该帧数据转换为二值图像，采用形态学滤波方法,剔除孤立点
            h=im2bw(temp);
            L=bwlabeln(h,8);
            s=regionprops(L,'Area');
            bw=ismember(L,find([s.Area]>=2));
            %获取滤波后的数据
            temp=uint8(temp.*double(bw));
            [r,c]=find(temp~=0);
             for i=1:length(r)
                 p_value(r(i,1),c(i,1))=temp(r(i,1),c(i,1));
             end
            
             %%
               %当前帧的有效值覆盖上一帧对应的位置
             %%
            smoother=Filtered(temp); 
            image=imresize(uint8(smoother),1);
            [px,py]=GetPressureCop(image);
            PressCop(j,:)=[px,py];
            [gx,gy]=GetGeometricCop(image);
            GeometricCop(j,:)=[gx,gy];           
end    
%  [prow,pcol]=find(p_value~=0);
%  [nrow,ncol]=find(number~=0);
%  for i=1:length(prow)
%      p_value(prow(i),pcol(i))=uint8(p_value(prow(i),pcol(i))/number(nrow(i),ncol(i)));
%  end
  %im=Filtered(p_value);
  footdata=p_value;
  image = label2rgb(gray2ind(uint8(p_value), 255), jet(255));
  figure;
  imshow(image);
  hold on;
  plot(PressCop(:,2),PressCop(:,1),'k-','LineWidth',5); 
  
end


