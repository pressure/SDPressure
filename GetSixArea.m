%%
%%
%计算每个脚底的六个区域的小矩形
function [f1,f2,f3,f4,f5,f6,f7,f8]=GetSixArea(MinRectPoints)
%MinRectPoints 逆时针顺序，由下方为第一个点
p1=MinRectPoints(1,:);
p2=MinRectPoints(2,:);
p3=MinRectPoints(3,:);
p4=MinRectPoints(4,:);
%%
%求宽度上的两个中点p1和p2 p3和p4
[c1,~]=GetUnformPoints(p1,p2,2);
[c2,~]=GetUnformPoints(p4,p3,2);
f7=c1;
f8=c2;
%%
%求长度上的两个三等分点，p1和p4,p2和p3
[f1,f2]=GetUnformPoints(p1,p4,3);
[f3,f4]=GetUnformPoints(p2,p3,3);
[f5,f6]=GetUnformPoints(c1,c2,3);
%%



end

function  [p1,p2]=GetUnformPoints(point1,point2,num)
%%
%%
%三等分
if num==3
if point1(1,1)>point2(1,1)
x1=point1(1,1)-abs(point2(1,1)-point1(1,1))/3.0;
x2=point1(1,1)-2*abs(point2(1,1)-point1(1,1))/3.0;
else
x1=point1(1,1)+abs(point2(1,1)-point1(1,1))/3.0;
x2=point1(1,1)+2*abs(point2(1,1)-point1(1,1))/3.0;
end
if point1(1,2)<point2(1,2)
y1=point1(1,2)+abs(point2(1,2)-point1(1,2))/3.0;
y2=point1(1,2)+2*abs(point2(1,2)-point1(1,2))/3.0;
else
y1=point1(1,2)-abs(point2(1,2)-point1(1,2))/3.0;
y2=point1(1,2)-2*abs(point2(1,2)-point1(1,2))/3.0;
end
p1=[x1 y1];
p2=[x2 y2];
end
%%
%二等分
if num==2
  if point1(1,1)>point2(1,1)
      x=point1(1,1)-abs(point2(1,1)-point1(1,1))/2.0;
  else
      x=point1(1,1)+abs(point2(1,1)-point1(1,1))/2.0;
  end
  if point1(1,2)<point2(1,2)
      y=point1(1,2)+abs(point2(1,2)-point1(1,2))/2.0;
  else
      y=point1(1,2)-abs(point2(1,2)-point1(1,2))/2.0;
  end
  p1=[x y];
  p2=p1;
end
end