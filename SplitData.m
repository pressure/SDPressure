function [Ldata,Rdata,u,re]=SplitData(Pdata)
[r,c]=find(Pdata~=0);
data=[r,c];
[u, re]=KMeans(data,2);
%[m, n]=size(re);
%最后显示聚类后的数据
% figure;
% rgb = label2rgb(gray2ind(uint8(Pdata), 255), jet(255));
% imshow(rgb);
% hold on;
% for i=1:m 
%     if re(i,3)==1   
%          plot(re(i,2),re(i,1),'k.');
%          hold on; 
%     else 
%          plot(re(i,2),re(i,1),'b.'); 
%          hold on;
%     end
% end


if u(1,2)>u(2,2) %说明类1是右脚
    [r1,~]=find(re(:,3)==1);
    [r2,~]=find(re(:,3)==2);
    index1=re(r1,1:2);
    index2=re(r2,1:2);
    Ldata=zeros(60,60);
    Rdata=zeros(60,60);
     for i=1:length(r1)
         Rdata(index1(i,1),index1(i,2))=Pdata(index1(i,1),index1(i,2));
     end   
     for i=1:length(r2)
         Ldata(index2(i,1),index2(i,2))=Pdata(index2(i,1),index2(i,2));
     end     
else %类1是左脚
    Ldata=zeros(60,60);
    Rdata=zeros(60,60);
    [r1,~]=find(re(:,3)==1);
    [r2,~]=find(re(:,3)==2);
    index1=re(r1,1:2);
    index2=re(r2,1:2);
      for i=1:length(r1)
         Ldata(index1(i,1),index1(i,2))=Pdata(index1(i,1),index1(i,2));
     end   
     for i=1:length(r2)
         Rdata(index2(i,1),index2(i,2))=Pdata(index2(i,1),index2(i,2));
     end     
end
%Lrgb = label2rgb(gray2ind(uint8(Ldata), 255), jet(255));
%Rrgb = label2rgb(gray2ind(uint8(Rdata), 255), jet(255));
%%
%可视化
% figure
% subplot(1,2,1);
% imshow(Lrgb);
% title('Left foot')
% 
% subplot(1,2,2);
% imshow(Rrgb);
% title('Right foot')
end

