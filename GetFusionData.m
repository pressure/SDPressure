%%
%对指定路径下的所有txt文档进行读取，并对数据进行融合，输出融合后的数据
function  [Pdata,file_num,PressCop,GeometricCop,row_sum,col_sum,p_sum]=GetFusionData(file_path,flag)

file_path_list=dir(strcat(file_path,'*.txt'));
file_num=length(file_path_list); 
 PressCop=[];
 GeometricCop=[];
% number=uint8(zeros(60,60));%统计每个压力点在采样时间内含有多少个数据值
 p_value=uint8(zeros(60,60));
 row_sum=zeros(60,file_num);
 col_sum=zeros(file_num,60);
 p_sum=zeros(1,file_num);
 for  j=1:file_num
           % nf=uint8(zeros(60,60));
            temp=load(strcat(file_path,file_path_list(j).name));
              %将该帧数据转换为二值图像，采用形态学滤波方法,剔除孤立点
            h=im2bw(temp);
            L=bwlabeln(h,8);
            s=regionprops(L,'Area');
            bw=ismember(L,find([s.Area]>=2));
            %获取滤波后的数据
            temp=uint8(temp.*double(bw));
            [r,c]=find(temp~=0);
            %均值放开
%              for i=1:length(r)
%                 nf(r(i,1),c(i,1))=1;
%              end
            % 覆盖前一帧放开
             for i=1:length(r)
                p_value(r(i,1),c(i,1))=temp(r(i,1),c(i,1));
             end
             %均值放开
%              number=number+nf;
%              p_value=p_value+temp;
             %%
               %当前帧的有效值覆盖上一帧对应的位置
               
             %%
             %计算平滑后的数据的几何中心和压力中心
           % smoother=Filtered(temp); 
            %image=imresize(uint8(temp),1);
            [px,py,rsum,csum,sum]=GetPressureCop(temp);
            row_sum(:,j)=rsum;
            col_sum(j,:)=csum;
            p_sum(1,j)=sum;
            PressCop(j,:)=[px,py];
            [gx,gy]=GetGeometricCop(temp);
            GeometricCop(j,:)=[gx,gy];           
 end  
%%
%获得均值放开
%  [prow,pcol]=find(p_value~=0);
%  [nrow,ncol]=find(number~=0);
%  for i=1:length(prow)
%      p_value(prow(i),pcol(i))=uint8(p_value(prow(i),pcol(i))/number(nrow(i),ncol(i)));
%  end
%可视化
if flag
   Pdata=Filtered(p_value);
 else
   Pdata=p_value;
end
% rgb = label2rgb(gray2ind(uint8(Pdata), 255), jet(255));
% h=figure('Visible','on');
% imshow(rgb);

end
