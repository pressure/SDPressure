clc;
clear;
RawData = load('.\sourcedata\DynamicData\NP0001_20170807144948_Raw.txt');
[m,n] = size(RawData);
for i = 1:5:n
    ii = floor(i/5) + 1;
    a(ii) = min(RawData(:,i));
end

minTime = RawData(1,1);
maxTime = RawData(m,1);
AllFrameData = zeros(60, 180, (maxTime-minTime)/10);

for ir = 1:m
    for ic = 1:5:n
        iTime     = (RawData(ir,ic) - minTime)/10 + 1;
        iNodeId   = RawData(ir,ic+1);
        iRow      = RawData(ir,ic+2) + 1;
        iCol      = RawData(ir,ic+3) + 1 + (iNodeId-1)*60;
        iPressure = RawData(ir,ic+4);
        AllFrameData(iRow,iCol,iTime) = iPressure;
    end
end
% for i=1:1:(maxTime-minTime)/10
%     pause(0.5);
%    imshow(AllFrameData(:,:,i));
%    hold on;
% end
