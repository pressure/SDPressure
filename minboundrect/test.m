%%
%footdata为脚型压力分布二维矩阵
%%
[r,c]=find(footdata~=0);
[recty,rectx,area,perimeter] =  minboundrect(c,r);  
[wei,hei] = minboxing(rectx(1:end-1),recty(1:end-1));