function   Psum=GetAreaSumPressures(data,AreaIndex)
sum=0;
[m,~]=size(AreaIndex);
for i=1:m
   sum=sum+data(AreaIndex(i,1),AreaIndex(i,2)); 
end
Psum=sum;
end